package com.jack.mainactivity;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class ProductsListAdapter extends RecyclerView.Adapter<ProductsListAdapter.ProductsListViewHolder> {

    /*
A Inner class for the view holde.
 */
    class ProductsListViewHolder extends RecyclerView.ViewHolder implements
            View.OnClickListener{

        TextView deleteTvInConfirmation;
        TextView cancelTvInConfirmation;

        TextView priceView;
        TextView quantityView;
        final TextView productNameView;
        Button plusProductBtn;
        Button minusProductBtn;
        Button deleteProductBtn;

        ProductsListViewHolder(View view){
            super(view);

            priceView = (TextView) view.findViewById(R.id.price_value);
            quantityView = (TextView) view.findViewById(R.id.quantity_value);
            productNameView = (TextView) view.findViewById(R.id.name_tag);
            plusProductBtn = (Button) view.findViewById(R.id.plus_quantity_btn);
            minusProductBtn = (Button) view.findViewById(R.id.minus_quantity_btn);
            deleteProductBtn = (Button) view.findViewById(R.id.x_button);

            deleteTvInConfirmation = (TextView) view.findViewById(R.id.btn_delete);
            cancelTvInConfirmation = (TextView) view.findViewById(R.id.btn_cancel);

            view.setOnClickListener(this);
        }

        /**
         * This gets called by the child views during a click. We fetch the date that has been
         * selected, and then call the onClick handler registered with this adapter, passing that
         * date.
         *
         * @param v the View that was clicked
         */
        @Override
        public void onClick(View v) {
            //int adapterPosition = getAdapterPosition();
            //mCursor.moveToPosition(adapterPosition);
            //...
        }
    }

    private static final int NORMAL_ITEM_VIEW = 1;
    private static final int DELETE_CONFIRM_VIEW = 2;

    //The list to show in the list.
    private List<Product> productsList;

    //list for waiting to delete items;
    private List<Product> deleteAppendingProducts;

    /* The context we use to utility methods, app resources and layout inflaters */
    private final Context mContext;

    /*
     * Below, we've defined an interface to handle clicks on items within this Adapter. In the
     * constructor of our ForecastAdapter, we receive an instance of a class that has implemented
     * said interface. We store that instance in this variable to call the onClick method whenever
     * an item is clicked in the list.
     */
    final private ProductsListOnClickHandler mClickHandler;

    public interface ProductsListOnClickHandler{
        void onClick();
    }

    ProductsListAdapter(final Context context, ProductsListOnClickHandler clickHandler, List<Product> productList) {
        this.mContext = context;
        this.mClickHandler = clickHandler;
        this.productsList = productList;
        deleteAppendingProducts = new ArrayList<>();
    }

    public ItemTouchHelper.SimpleCallback getSwipeCallback() {
        return new ItemTouchHelper.SimpleCallback
                (0, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {
            @Override
            public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
                return false;
            }

            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
                int position = viewHolder.getAdapterPosition();
                deleteAppendingProducts.add(productsList.get(position));
                notifyDataSetChanged();
            }
        };
    }

    /**
     * This gets called when each new ViewHolder is created. This happens when the RecyclerView
     * is laid out. Enough ViewHolders will be created to fill the screen and allow for scrolling.
     *
     * @param parent The ViewGroup that these ViewHolders are contained within.
     * @param viewType  If your RecyclerView has more than one type of item (like ours does) you
     *                  can use this viewType integer to provide a different layout. See
     *                  {@link android.support.v7.widget.RecyclerView.Adapter#getItemViewType(int)}
     *                  for more details.
     * @return A new ProductsListViewHolder that holds the View for each list item
     */
    @Override
    public ProductsListViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = null;

        if(viewType == NORMAL_ITEM_VIEW) {
            view = LayoutInflater.from(mContext).inflate(R.layout.products_list_item, parent, false);
        } else if (viewType == DELETE_CONFIRM_VIEW){
            view = LayoutInflater.from(mContext).inflate(R.layout.delete_product_layout, parent, false);
        }

        view.setFocusable(true);

        return new ProductsListViewHolder(view);
    }

    /**
     * OnBindViewHolder is called by the RecyclerView to display the data at the specified
     * position. In this method, we update the contents of the ViewHolder to display the weather
     * details for this particular position, using the "position" argument that is conveniently
     * passed into us.
     *
     * @param holder The ViewHolder which should be updated to represent the
     *                                  contents of the item at the given position in the data set.
     * @param position                  The position of the item within the adapter's data set.
     */
    @Override
    public void onBindViewHolder(final ProductsListViewHolder holder, int position) {
        //mCursor.moveToPosition(position);

        int viewType = getItemViewType(position);

        if(viewType == DELETE_CONFIRM_VIEW){

            holder.deleteTvInConfirmation.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = holder.getAdapterPosition();
                    Product product = productsList.get(position);
                    ((ShopActivity)mContext).deleteProduct(product);
                    deleteAppendingProducts.remove(product);
                }
            });

            holder.cancelTvInConfirmation.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //The product is no more target for delete
                    int position = holder.getAdapterPosition();
                    deleteAppendingProducts.remove(productsList.get(position));
                    notifyDataSetChanged();
                }
            });

        } else if(viewType == NORMAL_ITEM_VIEW) {
            position = holder.getAdapterPosition();
            holder.productNameView.setText((productsList.get(position).getName()));
            holder.quantityView.setText(productsList.get(position).getQuantity() + "");
            holder.priceView.setText(productsList.get(position).getPrice());
        /*
        Set the listener for minus product button
         */
            holder.minusProductBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    int position = holder.getAdapterPosition();

                    Product product = productsList.get(position);

                    int quantity = product.getQuantity();

                    if (quantity == 1) {
                        showDeleteAlert(position);
                    } else {
                        product.setQuantity(quantity - 1);
                        ((ShopActivity) mContext).changePrice(Double.parseDouble(product.getPrice()) * -1);
                    }

                    notifyItemChanged(position);
                }
            });

        /*
        Set the listener for the plus product button
         */
            holder.plusProductBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    int position = holder.getAdapterPosition();

                    Product product = productsList.get(position);

                    product.setQuantity(product.getQuantity() + 1);

                    ((ShopActivity) mContext).changePrice(Double.parseDouble(product.getPrice()));

                    notifyItemChanged(position);
                }
            });

            holder.deleteProductBtn.setOnClickListener(
                    new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            int position = holder.getAdapterPosition();
                            showDeleteAlert(position);
                        }
                    }
            );
        }
    }

    private void showDeleteAlert(final int position){
        new AlertDialog.Builder(mContext)
                //.setTitle("Title")
                .setMessage(R.string.delete_confirmation)
                .setIcon(android.R.drawable.ic_menu_delete)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        Product p = productsList.get(position);
                        //productsList.remove(position);
                        ((ShopActivity)mContext).deleteProduct(p);
                        Toast.makeText(mContext, "The item deleted from shopping list", Toast.LENGTH_SHORT).show();
                    }})
                .setNegativeButton(android.R.string.no, null).show();
    }

    @Override
    public int getItemCount() {
        if(productsList == null)
            return 0;
        return productsList.size();
    }

//    void swapCursor(Cursor cursor){
//        mCursor = cursor;
//        notifyDataSetChanged();
//    }

    @Override
    public int getItemViewType(int position) {
        if(deleteAppendingProducts.contains(productsList.get(position)))
            return DELETE_CONFIRM_VIEW;
        return NORMAL_ITEM_VIEW;
    }



}
