package com.jack.mainactivity;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.ImageFormat;
import android.graphics.SurfaceTexture;
import android.graphics.drawable.Drawable;
import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraCaptureSession;
import android.hardware.camera2.CameraCharacteristics;
import android.hardware.camera2.CameraDevice;
import android.hardware.camera2.CameraManager;
import android.hardware.camera2.CameraMetadata;
import android.hardware.camera2.CaptureRequest;
import android.hardware.camera2.params.StreamConfigurationMap;
import android.media.Image;
import android.media.ImageReader;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Vibrator;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.Log;
import android.util.SparseArray;
import android.util.SparseIntArray;
import android.view.Surface;
import android.view.TextureView;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.google.android.gms.vision.Frame;
import com.google.android.gms.vision.barcode.Barcode;
import com.google.android.gms.vision.barcode.BarcodeDetector;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class ShopActivity extends AppCompatActivity implements
        ProductsListAdapter.ProductsListOnClickHandler {

    private static final String TAG = ShopActivity.class.getSimpleName();
    private static final SparseIntArray ORIENTATIONS = new SparseIntArray();
    private static final int REQUEST_CAMERA_PERMISSION = 200;

    //value between 0-255, where 0 is fully transparent
    private static final int MAIN_BACKGROUND_OPACITY_LEVEL = 100;

    static {
        ORIENTATIONS.append(Surface.ROTATION_0, 90);
        ORIENTATIONS.append(Surface.ROTATION_90, 0);
        ORIENTATIONS.append(Surface.ROTATION_180, 270);
        ORIENTATIONS.append(Surface.ROTATION_270, 180);
    }

    protected CameraDevice cameraDevice;
    protected CameraCaptureSession cameraCaptureSessions;
    protected CaptureRequest.Builder captureRequestBuilder;
    private ImageReader reader;
    private Handler mBackgroundHandler;
    private HandlerThread mBackgroundThread;
    private BarcodeDetector barcodeDetector;
    private boolean shouldShowPreview;
    private TextureView textureView;
    private ArrayList<Product> productsList;
    private ProductsListAdapter mProductsListAdapter;
    private RecyclerView mRecyclerView;
    private TextView tvInstructions;
    private Button tvTotalPrice;
    private boolean shouldVibrate;

    //use to keep track of which barcodes added.
    private List<String> barcodesList;

    private double totalPrice;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        getSupportActionBar().hide();

        //This will be the camera preview
        textureView = (TextureView) findViewById(R.id.texture);
        assert textureView != null;
        textureView.setSurfaceTextureListener(textureListener);

        //Initialize the barcode detector
        barcodeDetector = new BarcodeDetector.Builder(this)
                .setBarcodeFormats(Barcode.ALL_FORMATS)
                .build();

        //Define the user settings (still didn't implemented setting choosing).
        shouldShowPreview = true;
        shouldVibrate = true;

        //Initialize the list which will contain all the barcodes added.
        productsList = new ArrayList<>();
        barcodesList = new ArrayList<>();

        //Recycler view initialize
        recyclerViewInit();

        //set the adapter
        mProductsListAdapter = new ProductsListAdapter(this, this, productsList);
        mRecyclerView.setAdapter(mProductsListAdapter);

        ItemTouchHelper touchHelper = new ItemTouchHelper(mProductsListAdapter.getSwipeCallback());
        touchHelper.attachToRecyclerView(mRecyclerView);

        //Set the main background picture
        Drawable main_background = getDrawable(R.drawable.supermarket_background);
        main_background.setAlpha(MAIN_BACKGROUND_OPACITY_LEVEL);
        LinearLayout mainLayout = (LinearLayout) findViewById(R.id.main_layout);
        mainLayout.setBackground(main_background);

        //the instructions text view will only show if the list is empty
        tvInstructions = (TextView) findViewById(R.id.tv_instructions);
        setInstructionsVisibility();

        //The view to show the total price
        tvTotalPrice = (Button) findViewById(R.id.btn_total_price);
        totalPrice = 0;
    }

    public void recyclerViewInit(){
        mRecyclerView = (RecyclerView) findViewById(R.id.recycler_view_products);
        LinearLayoutManager layoutManager =
                new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        mRecyclerView.setLayoutManager(layoutManager);
        mRecyclerView.setHasFixedSize(true);
    }

    /*
        The camera preview listener
     */
    TextureView.SurfaceTextureListener textureListener = new TextureView.SurfaceTextureListener() {
        @Override
        public void onSurfaceTextureAvailable(SurfaceTexture surface, int width, int height) {
            //open your camera here
            openCamera();
        }

        @Override
        public void onSurfaceTextureSizeChanged(SurfaceTexture surface, int width, int height) {
            // Transform you image captured size according to the surface width and height
        }

        @Override
        public boolean onSurfaceTextureDestroyed(SurfaceTexture surface) {
            return false;
        }

        @Override
        public void onSurfaceTextureUpdated(SurfaceTexture surface) {
        }
    };

    /*
        A call back for the camera state.
     */
    private final CameraDevice.StateCallback stateCallback = new CameraDevice.StateCallback() {
        @Override
        public void onOpened(@NonNull CameraDevice camera) {
            //This is called when the camera is open
            Log.e(TAG, "onOpened");
            cameraDevice = camera;
            analyzeVideo();
        }

        @Override
        public void onDisconnected(@NonNull CameraDevice camera) {
            camera.close();
            cameraDevice = null;
        }

        @Override
        public void onError(@NonNull CameraDevice camera, int error) {
            camera.close();
            cameraDevice = null;
        }
    };

    /*
        Create background thread for different actions to do with the pictures so
        we will not block the UI.
     */
    protected void startBackgroundThread() {
        mBackgroundThread = new HandlerThread("Camera Background");
        mBackgroundThread.start();
        mBackgroundHandler = new Handler(mBackgroundThread.getLooper());
    }

    protected void stopBackgroundThread() {
        mBackgroundThread.quitSafely();
        try {
            mBackgroundThread.join();
            mBackgroundThread = null;
            mBackgroundHandler = null;
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    /*
        In this method we will analyze what the video camera is seeing. we will accomplish two tasks:
        1. we will show the camera preview if the user chooses to do so.
        2. we will send the picture analyzes to the barcode scanner to get result.
     */
    protected void analyzeVideo() {
        try {
            //The list of of surfaces. we will have 2 surfaces, one for camera preview and one for analyze.
            final List<Surface> outputSurfaces = new ArrayList<>();
            captureRequestBuilder = cameraDevice.createCaptureRequest(CameraDevice.TEMPLATE_PREVIEW);
            //captureRequestBuilder.set(CaptureRequest.LENS_FOCAL_LENGTH,(float)50);

            //take care of camera preview if needed.
            if (shouldShowPreview) {
                SurfaceTexture texture = textureView.getSurfaceTexture();
                assert texture != null;
                texture.setDefaultBufferSize(textureView.getWidth(), textureView.getHeight());
                Surface surface = new Surface(texture);
                outputSurfaces.add(surface);
                captureRequestBuilder.addTarget(surface);
            }

            //take care of barcode-scanner surface
            reader = ImageReader.newInstance(500, 700, ImageFormat.JPEG, 3);
            reader.setOnImageAvailableListener(onImageAvailableListener, null);
            outputSurfaces.add(reader.getSurface());
            captureRequestBuilder.addTarget(reader.getSurface());

            //Create the actual capturing session.
            cameraDevice.createCaptureSession(outputSurfaces, new CameraCaptureSession.StateCallback() {
                @Override
                public void onConfigured(@NonNull CameraCaptureSession cameraCaptureSession) {

                    //The camera is already closed
                    if (null == cameraDevice) {
                        return;
                    }

                    // When the session is ready, we will start displaying the preview.
                    cameraCaptureSessions = cameraCaptureSession;

                    //Set the auto focus mode.
                    captureRequestBuilder.set(CaptureRequest.CONTROL_AF_MODE,
                            CameraMetadata.CONTROL_AF_MODE_CONTINUOUS_PICTURE);

                    try {
                        cameraCaptureSessions.setRepeatingRequest(
                                captureRequestBuilder.build(),
                                null,
                                mBackgroundHandler);
                    } catch (CameraAccessException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onConfigureFailed(@NonNull CameraCaptureSession cameraCaptureSession) {
                    Toast.makeText(ShopActivity.this, "Configuration change", Toast.LENGTH_SHORT).show();
                }
            }, null);

        } catch (CameraAccessException e) {
            e.printStackTrace();
        }

    }

    public void deleteProduct(Product product) {
        String id = product.getBarcodeNum();
        int position = productsList.indexOf(product);
        if (productsList.contains(product)) {
            productsList.remove(product);
        }
        if (barcodesList.contains(id)) {
            barcodesList.remove(id);
        }
        mProductsListAdapter.notifyItemRemoved(position);
        changePrice(Double.parseDouble(product.getPrice())
                * product.getQuantity() * -1);
    }

    /*
    choose when the show the text view with the instructions.
    only show when the user doesn't have any product on the list.
     */
    private void setInstructionsVisibility() {
        if (productsList == null || productsList.size() == 0) {
            tvInstructions.setVisibility(View.VISIBLE);
        } else {
            tvInstructions.setVisibility(View.GONE);
        }
    }

    /*
        This method called when each frame is ready.
        should be optimized so maybe it will not process each and every frame or other optimizations.
     */
    ImageReader.OnImageAvailableListener onImageAvailableListener = new ImageReader.OnImageAvailableListener() {
        @Override
        public void onImageAvailable(ImageReader reader) {

            final Image image = reader.acquireLatestImage();

            if (image == null)
                return;

            final ByteBuffer buffer = image.getPlanes()[0].getBuffer();

            final byte[] bytes = new byte[buffer.capacity()];

            buffer.get(bytes);

            Bitmap bitmap = BitmapFactory.decodeByteArray(bytes, 0, bytes.length, null);

            Frame frame = new Frame.Builder()
                    .setBitmap(bitmap)
                    .build();

            SparseArray<Barcode> barcodes = barcodeDetector.detect(frame);

            /*
            Barcode found
             */
            if (barcodes.size() > 0 && !barcodesList.contains(barcodes.valueAt(0).displayValue)) {

                Barcode foundedBarcode = barcodes.valueAt(0);

                //I add it here so it will go first in the list before i add it again.
                //need to be optimized.
                barcodesList.add(foundedBarcode.displayValue);

                Log.i(TAG, "I found the barcode " + foundedBarcode.displayValue);

                handleBarcodeDetection(foundedBarcode);
            }

            image.close();
        }
    };

    /*
    once we found a new barcode from the camera, he we will handle it.
     */
    public void handleBarcodeDetection(Barcode barcode) {

        if (shouldVibrate)
            makeVibration();

        addProduct(barcode.displayValue);

        Toast.makeText(this, barcode.displayValue + " found", Toast.LENGTH_SHORT).show();

        //remove instructions
        setInstructionsVisibility();
    }

    public void changePrice(double difference) {
        totalPrice += difference;
        tvTotalPrice.setText(totalPrice + "");

    }

    public void addProduct(String id) {
        //getProductFromDB(id) need to be implemented
        String productPrice = "9.5";
        String productName = id;
        Product product = new Product(productName, productName, productPrice);

        changePrice(Double.parseDouble(product.getPrice()));

        //we want the last product scanned to be the first in the list
        productsList.add(0, product);

        mProductsListAdapter.notifyItemInserted(0);
    }

    private void makeVibration() {
        ((Vibrator) Objects.requireNonNull(getSystemService(VIBRATOR_SERVICE))).vibrate(100);

        //Code for when upgrading to target API 26+
        /*
        if (Build.VERSION.SDK_INT >= 26) {
            ((Vibrator) getSystemService(VIBRATOR_SERVICE)).vibrate(VibrationEffect.createOneShot(150, VibrationEffect.DEFAULT_AMPLITUDE));
        } else {
            ((Vibrator) getSystemService(VIBRATOR_SERVICE)).vibrate(150);
        }
        */
    }

    private void openCamera() {
        CameraManager manager = (CameraManager) getSystemService(Context.CAMERA_SERVICE);
        Log.e(TAG, "is camera open");
        try {
            assert manager != null;
            String cameraId = manager.getCameraIdList()[0];
            CameraCharacteristics characteristics = manager.getCameraCharacteristics(cameraId);
            StreamConfigurationMap map = characteristics.get(CameraCharacteristics.SCALER_STREAM_CONFIGURATION_MAP);
            assert map != null;
            // Add permission for camera and let user grant the permission
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(ShopActivity.this, new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_CAMERA_PERMISSION);
                return;
            }
            manager.openCamera(cameraId, stateCallback, null);
        } catch (CameraAccessException e) {
            e.printStackTrace();
        }
        Log.e(TAG, "openCamera X");
    }

    private void closeCamera() {
        if (null != cameraDevice) {
            cameraDevice.close();
            cameraDevice = null;
        }
        if (null != reader) {
            reader.close();
            reader = null;
        }
        if (cameraCaptureSessions != null) {
            cameraCaptureSessions.close();
            cameraCaptureSessions = null;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == REQUEST_CAMERA_PERMISSION) {
            if (grantResults[0] == PackageManager.PERMISSION_DENIED) {
                // close the app
                Toast.makeText(ShopActivity.this, "Sorry!!!, you can't use this app without granting permission", Toast.LENGTH_LONG).show();
                finish();
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.e(TAG, "onResume");
        startBackgroundThread();
        if (textureView.isAvailable()) {
            openCamera();
        } else {
            textureView.setSurfaceTextureListener(textureListener);
        }
    }

    @Override
    protected void onPause() {
        Log.e(TAG, "onPause");
        closeCamera();
        stopBackgroundThread();
        super.onPause();
    }

    /*
    Method to decide what happens when the user click on an item in the recycler-view.
     */
    @Override
    public void onClick() {

    }
}